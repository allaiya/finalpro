import React from 'react';
import axios from 'axios';
import 'bootstrap-css-only/css/bootstrap.min.css';

class Products extends React.Component {
  constructor() {
    super();
    this.state = {
      list : []
    }
  }
  componentDidMount() {
    console.log("componentDidMount");
    

    axios.get('http://127.0.0.1:8000/api/products/').then(res => {
      const rest = res.data;
      this.setState({ list });
      console.log(res.data)
    });


  }
  render() {

    return (
      <div className="container">
        <br></br>
        <br></br>
        <div className="row">
          
          
          {this.state.list.map((item, index) => (
            
            <div className="col-md-4">
            <div className="card" style={{marginBottom:"20px"}} >
                
              <img className="card-img-bottom" src={item.image} alt="Card image cap" style={{ height: '500px',width: 'auto'}} ></img>
              <div className="card-body">
               <h1 className="card-text text-dark " >{item.title}</h1>
                <p className="card-text display-2 ">{item.description}</p>
                
                <a href="#" className="btn btn-warning">Add to Cart</a>
                <span className="lead"><span className="badge badge-primary">${item.price}</span></span>
              </div>
            </div>
            </div>
          ))}
        </div>
      </div>

    );
  }
}
export default Products;

