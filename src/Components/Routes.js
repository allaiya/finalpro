import React from 'react';
import { BrowserRouter as Router,Switch,Route,Link} from 'react-router-dom';

import Products from './Products';
import Login from './Login';
import Register from './Register';
import Orders from './Orders';


const Routes = () =>{

    return(
        <Router>
            <div>
           
            <div class="pos-f-t">
            <div class="collapse" id="navbarToggleExternalContent">
              <div class="bg-dark p-4">
                <h4 class="text-white">Amazon</h4>
                <span class="text-muted">Toggleable via the navbar brand.</span>
              </div>
            </div>
            <nav class="navbar navbar-dark bg-dark">
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><Link to='/Products'>Products</Link></span><br></br>
                <span class="navbar-toggler-icon"><Link to='/LogIn'>Log In</Link></span><br></br>
                <span class="navbar-toggler-icon"><Link to='/Register'>Register</Link></span><br></br>
                <span class="navbar-toggler-icon"><Link to='/Orders'>Orders</Link></span><br></br>

              </button>
            </nav>
          </div>


                

                    
                    
                    <Switch>
                    <Route path="/Login"><Login /></Route> 
                    
                    <Route path="/Register"><Register /></Route>
                    <Route path="/Orders"><Orders /></Route>
                    <Route path="/"><Products /></Route> 

                
                    {/* <Route path="/*"><NotFound></NotFound></Route> */}
                    <Route component={() => (<div>404 Not found </div>)} />
                    </Switch>
               
            </div>
            
        </Router>
    );
}



export default Routes;